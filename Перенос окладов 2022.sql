select * from TEMP_IMPORT_WAGES w  where Post like '%����������� ������������ ������%'

drop table TEMP_IMPORT_WAGES

create table TEMP_IMPORT_WAGES (DEPARTMENT varchar2(1500), POST varchar2(1000),  NEWWAGE varchar2(50), OLDWAGE varchar2(50), COEFF varchar2(20))

select * from TEMP_IMPORT_WAGES where newwage is null and oldwage is null and coeff is null and post is null

delete from TEMP_IMPORT_WAGES where newwage is null and oldwage is null and coeff is null and post is null

-- �������� ������� ��������� ��� � ������� ����������
select ps.do_act_from, ps.do_act_to, ps.psdep_code, ps.psdep_name, pt.code, pt.name, g.do_act_from, g.do_act_to, g.clnrate
from CLNPSDEPGS g, CLNPSDEP ps, clnposts pt
where ps.postrn = pt.rn and g.prn = ps.rn
and g.GRSALARY = 113250213 and (g.do_act_to is null or g.do_act_to>to_date('01.10.2022','dd.mm.yyyy'))
--and g.do_act_from>=to_date('01.10.2022','dd.mm.yyyy')
and (ps.do_act_to is null or ps.do_act_to>to_date('01.10.2022','dd.mm.yyyy'))

select * from CLNPSDEP ps 
where (ps.do_act_to is null or ps.do_act_to>=to_date('01.10.2022','dd.mm.yyyy'))
and not exists (select 1 from CLNPSDEPGS g where g.GRSALARY = 113250213 and g.prn = ps.rn)
and exists (select 1 from clnpspfm f where f.psdeprn = ps.rn and (f.endeng is null or f.endeng>=to_date('01.10.2022','dd.mm.yyyy')))

-- ��������� ��������������� ��������� �������
select oldwage, count(distinct newwage) from TEMP_IMPORT_WAGES 
group by oldwage

-- ���������� ������ ������� �� �������� - ������/�����
--1) ������ � ����������� �������� ��� �� 01.10.2022
declare
nNewrate number(12,2);
nnum number;
begin
  nnum := 0;
  for k in (
    select ps.rn, g.crn, g.do_act_from, g.do_act_to, g.clnrate
    from CLNPSDEPGS g, CLNPSDEP ps, clnposts pt, ins_department d
    where ps.postrn = pt.rn and g.prn = ps.rn
    and g.GRSALARY = 113250213 and ps.deptrn = d.rn
    and (g.do_act_to is null or g.do_act_to>to_date('01.10.2022','dd.mm.yyyy')) 
    and g.do_act_from<to_date('01.10.2022','dd.mm.yyyy')
    and (ps.do_act_to is null or ps.do_act_to>to_date('01.10.2022','dd.mm.yyyy'))
    --and exists (select 1 from CLNPSDEPGS g1 where g1.prn = g.prn and g1.do_act_from>=to_date('01.10.2022','dd.mm.yyyy') and g1.GRSALARY = 113250213)
  ) loop
  
    nNewrate := null;

    begin
      select distinct to_number(w.newwage) into nNewrate from TEMP_IMPORT_WAGES w 
      where to_number(w.oldwage) = k.clnrate;
    exception
      when NO_DATA_FOUND then
        nNewrate := null;
        dbms_output.put_line(k.clnrate);
    end;
    
    if not nNewrate is null then
      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), k.rn, 24404, k.rn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, nNewrate, nNewrate);
      nnum := nnum+1;
    end if;
  
  end loop;
  
  dbms_output.put_line(nnum);

end;

-- ������������� ������������

select * from CLNPSDEPGS where GRSALARY = 113250213 and clnrate=summ and do_act_from = to_date('01.10.2022','dd.mm.yyyy') and coeffic = 0
update CLNPSDEPGS set coeffic = 0
where GRSALARY = 113250213 and clnrate=summ and do_act_from = to_date('01.10.2022','dd.mm.yyyy') 
and coeffic = 0


--2) �������� ������� ��� 30,09,2022
select * from CLNPSDEPGS
where GRSALARY = 113250213 
and do_act_from<to_date('01.10.2022','dd.mm.yyyy')
and (do_act_to is null or do_act_to>to_date('01.10.2022','dd.mm.yyyy'))

update CLNPSDEPGS set do_act_to = to_date('30.09.2022','dd.mm.yyyy')
where GRSALARY = 113250213 
and do_act_from<to_date('01.10.2022','dd.mm.yyyy')
and (do_act_to is null or do_act_to>to_date('01.10.2022','dd.mm.yyyy'))


-- �� ������������ ������
11300
11300
12000
9700
0
--

--3) ������� ��� � ���������� ����������
--�) ��������� �� � ��
select * from CLNPSPFMGS where GRSALARY = 113250213

begin

  for k in ( 
    select distinct f.rn, f.crn, g.clnrate, 
    D_MAX2(to_date('01.10.2022','dd.mm.yyyy'), f.begeng) as datebegin
    from clnpspfm f, clnpsdep ps, CLNPSDEPGS g
    where f.psdeprn = ps.rn
    and (f.endeng is null or f.endeng > to_date('01.10.2022','dd.mm.yyyy'))
    and g.prn = ps.rn and g.GRSALARY = 113250213
    and g.do_act_from=to_date('01.10.2022','dd.mm.yyyy')
    and not exists (
        select 1 from CLNPSPFMGS m 
        where m.prn = f.rn and m.grsalary = 113250213
        and m.do_act_from>=to_date('01.10.2022','dd.mm.yyyy')
    )
    order by 1
  ) loop
  
    insert into CLNPSPFMGS (rn, prn, Company, crn, grsalary, Do_Act_From, clnrate, coeffic, summ, note)
    values (gen_id(), k.rn, 24404, k.rn, 113250213, k.datebegin, k.clnrate, 0, k.clnrate, '������ 01012022');
  
  end loop;

end;

delete from CLNPSPFMGS where note = '������ 01012022'

-- ��������� ������ ��������� ���
begin
  for k in (
    select g.rn, g.do_act_from, g.do_act_to 
    from CLNPSPFMGS g
    where g.GRSALARY = 113250213
    and g.do_act_from>=to_date('01.10.2022','dd.mm.yyyy')
    and nvl(g.note,'-')<>'������ 01012022'
    and exists (
        select 1 from CLNPSPFMGS g1 
        where g1.grsalary = g.grsalary
        and g1.prn = g.prn
        and g1.note='������ 01012022'
    )
  )
  loop
    
    if k.do_act_from>=to_date('01.10.2022','dd.mm.yyyy') then
       update CLNPSPFMGS set note = '�������' where rn = k.rn;
    else
       update CLNPSPFMGS set do_act_to = to_date('30.09.2022','dd.mm.yyyy') where rn = k.rn;
    end if;

  end loop;
end;


begin
  for k in (
    select g.rn, g.do_act_from, g.do_act_to 
    from CLNPSPFMGS g
    where g.GRSALARY = 113250213
    and g.do_act_from<to_date('01.10.2022','dd.mm.yyyy')
    and (g.do_act_to>to_date('01.10.2022','dd.mm.yyyy') or g.do_act_to is null)
    and nvl(g.note,'-')<>'������ 01012022'
    and exists (
        select 1 from CLNPSPFMGS g1 
        where g1.grsalary = g.grsalary
        and g1.prn = g.prn
        and g1.note='������ 01012022'
    )
  )
  loop
    
    if k.do_act_from>=to_date('01.10.2022','dd.mm.yyyy') then
       update CLNPSPFMGS set note = '�������' where rn = k.rn;
    else
       update CLNPSPFMGS set do_act_to = to_date('30.09.2022','dd.mm.yyyy') where rn = k.rn;
    end if;

  end loop;
end;


-- ������� � ���� �� ���������� ����� ����� � 01.10.2022 � ������� ����������

select * from TEMP_IMPORT_WAGES where lower(post) like '%�������%'

select d.code, d.name, ps.psdep_code, ps.psdep_name 
from clnpsdep ps, ins_department d  
where not exists (
   select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
   and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
)
and ps.deptrn = d.rn
and ps.do_act_to is null
order by 3

select * from TEMP_IMPORT_WAGES w where lower(w.post) like '%�����%'

-- ��������� �� ��������������

select * from TEMP_IMPORT_WAGES w where w.department like '%COVID-19%' and lower(w.post) like '%����%'

begin
  for i in (
      select distinct ps.rn, ps.crn, d.name, ps.psdep_code, ps.psdep_name, w.department, w.newwage
      from clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w  
      where not exists (
         select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
         and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
      )
      and ps.deptrn = d.rn
      and ps.do_act_to is null
      /*
      and w.post like '�������%'
      and ps.psdep_name like '�������%'
      */
      and lower(ps.psdep_name) like '%����%'
      and lower(d.name) like '%��������������%'
      and w.department like '%COVID-19%'
      and lower(w.post) like '%����%'
      /*
      and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
      and (
          Instr(lower(replace(w.department,' ','')),lower(replace(d.name,' ','')))>0
          or 
          Instr(lower(replace(w.department,' ','')),lower(replace(d1.name,' ','')))>0
      )
      and (
        (lower(d.name) like '%����%' and lower(w.department) like '%����%')
        and 
        (not lower(w.department) like '%����� ����� ���������%' and lower(ps.psdep_name) like '%������%' )
        not lower(w.department) like '%����%'
        and not lower(ps.psdep_name) like '%������%'
      )
      */
  ) loop

      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, i.newwage, i.newwage);

  end loop;
end;

select Instr(lower(replace(w.department,' ','')),lower(replace(d1.name,' ',''))) 
from TEMP_IMPORT_WAGES w where w.rowid = 'AAAhvCAAEAAHzyjAAE'

/*and not exists (
   select 1 from TEMP_IMPORT_WAGES w1 where replace(replace(lower(w1.post), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
   and Instr(lower(trim(w1.department)),lower(trim(d1.name)))>0
   and trim(w1.newwage)<>trim(w.newwage)  --w1.rowid <> w.rowid
)*/
order by 1, 2




--------------------------------------------------------------------
-- �������� ���������� ������ �� ���� ��������
begin
  for i in (
    select ps.rn, ps.crn, d.code, d.name, ps.psdep_code, ps.psdep_name, w.department, w.newwage
    from clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w  
    where not exists (
       select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
       and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
    )
    and ps.deptrn = d.rn
    and ps.do_act_to is null
    and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
    /*and not exists (
       select 1 from TEMP_IMPORT_WAGES w1 where replace(replace(lower(w1.post), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
       and trim(w1.newwage)<>trim(w.newwage)  --w1.rowid <> w.rowid
    )*/
    order by 5, 6
  ) loop

      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, i.newwage, i.newwage);

  end loop;
end;
----------------------------------------------------------------------
-- �������� --
begin
  for i in (
    select ps.rn, ps.crn, d.code, d.name, ps.psdep_code, ps.psdep_name, w.department, w.newwage
    from clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w  
    where not exists (
       select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
       and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
    )
    and ps.deptrn = d.rn
    and ps.do_act_to is null
    and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
    and Instr(lower(trim(w.department)),lower(trim(d.name)))>0
    /*and not exists (
       select 1 from TEMP_IMPORT_WAGES w1 where replace(replace(lower(w1.post), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
       and Instr(lower(trim(w1.department)),lower(trim(d.name)))>0
       and trim(w1.newwage)<>trim(w.newwage)  --w1.rowid <> w.rowid
    )*/
    order by 1, 2
  ) loop

      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, i.newwage, i.newwage);

  end loop;
end;

-- ������� �� ������������ ��������������
begin
  for i in (
    select ps.rn, ps.crn, d1.code, d1.name, d.name, ps.psdep_code, ps.psdep_name, w.department, w.newwage
    from clnpsdep ps, ins_department d, ins_department d1, TEMP_IMPORT_WAGES w  
    where not exists (
       select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
       and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
    )
    and ps.deptrn = d.rn
    and d.prn = d1.rn
    and ps.do_act_to is null
    and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
    and Instr(lower(trim(w.department)),lower(trim(d1.name)))>0
    /*and not exists (
       select 1 from TEMP_IMPORT_WAGES w1 where replace(replace(lower(w1.post), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
       and Instr(lower(trim(w1.department)),lower(trim(d1.name)))>0
       and trim(w1.newwage)<>trim(w.newwage)  --w1.rowid <> w.rowid
    )*/
    order by 1, 2
  ) loop

      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, i.newwage, i.newwage);

  end loop;
end;



select * from TEMP_IMPORT_WAGES w order by POST


-- ���� ���������������
select distinct ps.rn, d.code, d.name, ps.psdep_code, ps.psdep_name, w.newwage from 
clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w,
(
select ps.rn, count(*) 
from clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w  
where not exists (
   select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
   and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
)
and ps.deptrn = d.rn
and ps.do_act_to is null
and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
group by ps.rn
having count(*)>1
) s1
where s1.rn = ps.rn
and ps.deptrn = d.rn
and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
order by 2,4


insert into TEMP_WAGES_DEPTYPES (deptrn, depname)
select distinct d.rn, d.name from 
clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w,
(
select ps.rn, count(*) 
from clnpsdep ps, ins_department d, TEMP_IMPORT_WAGES w  
where not exists (
   select 1 from CLNPSDEPGS g where g.prn = ps.rn and g.GRSALARY = 113250213
   and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
)
and ps.deptrn = d.rn
and ps.do_act_to is null
and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
group by ps.rn
having count(*)>1
) s1
where s1.rn = ps.rn
and ps.deptrn = d.rn
and replace(replace(lower(ps.psdep_name), ' ',''),'-','') = replace(replace(lower(w.post), ' ',''),'-','')
order by 1

drop table TEMP_WAGES_DEPTYPES 
create table TEMP_WAGES_DEPTYPES (deptrn number(17), depname varchar(450), wage number(10,2))

select * from TEMP_WAGES_DEPTYPES for update

-- ������� ��� � �����������, ��� ����� �� ���� ��� � ������� ����������
select * 
from CLNPSPFMGS g, clnpspfm f, clnpsdep ps
where g.GRSALARY = 113250213
and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
and g.prn = f.rn and f.psdeprn = ps.rn
and not exists (
select 1 from CLNPSDEPGS g1 where g1.Grsalary = g.grsalary
and g1.prn = ps.rn and g1.do_act_from = g.do_act_from
)

--1) ������� ������� ���������, � ������� ��� ���, �� ���� ��� � ��������� �����������
declare
nSum number;
begin
   for i in (
    select ps.rn, ps.crn, d.code, d.name, ps.psdep_code, ps.psdep_name 
    from clnpsdep ps, ins_department d  
    where not exists (
      select 1 from CLNPSDEPGS g 
      where g.prn = ps.rn and g.GRSALARY = 113250213
      and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
    )
    and exists (
      select 1 from CLNPSPFMGS g, clnpspfm f
      where g.GRSALARY = 113250213
      and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
      and g.prn = f.rn and f.psdeprn = ps.rn
    )
    and ps.deptrn = d.rn
    and ps.do_act_to is null
    ) loop
    
      begin
      select distinct g.summ into nSum from CLNPSPFMGS g, clnpspfm f
      where g.GRSALARY = 113250213
      and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
      and g.prn = f.rn and f.psdeprn = i.rn;
      exception
        when others then
        dbms_output.put_line(i.rn);  
      end;  
      
      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, nSum, nSum);
    
    end loop;
end;

--2) ������� ������� ���������, � ������� ��� ���, �� ���� ��� � ����������� � ������ �� �����������, � ��� ���������������
declare
nSum number;
begin
   for i in (
    select ps.rn, ps.crn, d.code, d.name, ps.psdep_code, ps.psdep_name 
    from clnpsdep ps, ins_department d  
    where not exists (
      select 1 from CLNPSDEPGS g 
      where g.prn = ps.rn and g.GRSALARY = 113250213
      and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
    )
    and exists (
      select 1 from CLNPSPFMGS g1, clnpspfm f1, clnpsdep ps1
      where g1.GRSALARY = 113250213
      and g1.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
      and g1.prn = f1.rn and f1.psdeprn = ps1.rn
      and ps1.psdep_code = ps.psdep_code -- ��������� �� ������������ ������� ���������
      and not exists ( -- ����� ����� ����������
        select 1 from CLNPSPFMGS g2, clnpspfm f2, clnpsdep ps2
        where g2.GRSALARY = 113250213
        and g2.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
        and g2.prn = f2.rn and f2.psdeprn = ps2.rn
        and ps2.psdep_code = ps1.psdep_code
        and ps2.rn <> ps1.rn
        and g2.summ <> g1.summ
      )
    )
    and ps.deptrn = d.rn
    and ps.do_act_to is null
    order by 5
    ) loop
    
      begin
        select distinct g.summ into nSum from CLNPSPFMGS g, clnpspfm f
        where g.GRSALARY = 113250213
        and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
        and g.prn = f.rn and f.psdeprn = i.rn;
      exception
        when others then
        dbms_output.put_line(i.rn);  
      end;  
      
      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, nSum, nSum);
    
    end loop;
end;


-- ��������� ������ �� �������� �� �������
begin
  for i in (
    select distinct ps.rn, ps.crn, p.num_value 
    from slcompcharges ch, slpays s, slpaysprm p, clnpspfm f, clnpsdep ps
    where s.slcompcharges = ch.rn
    and p.prn = s.rn
    and s.clnpspfm = f.rn
    and f.psdeprn = ps.rn
    and p.code = 'RATEVALUE'
    and s.year = 2022 and s.month = 10
    and (ch.code like '�����%' or ch.code like '���%')
    and not exists (
      select 1 from CLNPSDEPGS g 
      where g.prn = ps.rn and g.GRSALARY = 113250213
      and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
    )
    and exists (
      select 1 from TEMP_IMPORT_WAGES w where w.newwage = p.num_value
    )
    order by 1
  )
  loop

      insert into CLNPSDEPGS (rn, prn, company, crn, grsalary, do_act_from, Do_Act_To, clnrate, summ)
      values (gen_id(), i.rn, 24404, i.crn, 113250213, to_date('01.10.2022','dd.mm.yyyy'), null, i.num_value, i.num_value);
    
  end loop;
end;


-- ������� ��� ������� ���������� �� ����������
select ps.psdep_code, g.summ 
from CLNPSDEPGS g, clnpsdep ps
where g.grsalary = 113250213 and g.do_act_from = to_date('01.10.2022','dd.mm.yyyy')
and g.prn = ps.rn
and exists (
select 1 from CLNPSDEPGS g1 where g1.grsalary = 113250213 
and g1.do_act_from = to_date('01.10.2022','dd.mm.yyyy') 
and g1.prn = g.prn 
and g1.summ = g.summ
and g1.rn<>g.rn
)


