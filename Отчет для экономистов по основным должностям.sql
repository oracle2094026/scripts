drop table temp_report2022
create table temp_report2022 (pfmrn_main number(17), pfmrn_comb number(17), depname varchar2(250), postname varchar2(250), 
ratevol number(10,5), koeff number(10,5), summmain number(12,2), summcomb number(12,2), 
mainbeg date, mainend date, combbeg date, combend date
)

drop table temp_report_main;
create table temp_report_main(persrn number(17), pfmrn_main number(17), 
depname varchar2(250), postname varchar2(250), isexternal number(1),  
wfact number(10,2), wnorma number(10,2), 
ratevol number(10,5), summmain number(12,2), mainbeg date, mainend date
); 

drop table temp_report_comb;
create table temp_report_comb(persrn number(17), pfmrn_main number(17), pfmrn_comb number(17), 
depname varchar2(250), postname varchar2(250), 
ratevol number(10,5), koeff number(10,5), summcomb number(12,2), 
combbeg date, combend date, mainbeg date, mainend date
); 

--drop table temp_comb_distrib;
create table temp_comb_distrib(persrn number(17), pfmrn_main number(17), pfmrn_comb number(17), 
depname varchar2(250), postname varchar2(250), 
ratevol number(10,5), koeff number(10,5), summcomb number(12,2), 
combbeg date, combend date, mainbeg date, mainend date
); 



-- ������ ��������
declare 
dBegin date;
dEnd date;
nYear number;
nMonthBegin number;
nMonthEnd number;
nKoeffNumb number(10,5);
nSummMain number(12,2); 
nSummComb number(12,2); 
dBeginMain date;
dEndMain date;
sDOL varchar2(250);
nFact number(10,2);
nNorma number(10,2);
begin
  dBegin := to_date('01.01.2023','dd.mm.yyyy');
  dEnd := to_date('31.03.2023','dd.mm.yyyy');
  
  nYear := extract(year from dBegin);
  nMonthBegin := extract(month from dBegin);
  nMonthEnd := extract(month from dEnd);
  
  delete from temp_report_main;
  commit;
  
  for i in (
    select f.persrn, f.rn as main_rn, d.code as depcode, 
    nvl(ps.postrn, pt.rn) as postrn, ps.psdep_name,
    f.begeng as mainbeg, f.endeng as mainend, 
    (case when Instr('����.���-��;������� ������������', t.code)>0 then 1 else 0 end) as isexternal, 
    (select max(schedule) from clnpspfmhs hs where hs.prn = f.rn) as schedule
    from clnpspfm f
    inner join clnpspfmtypes t on (f.clnpspfmtypes = t.rn and t.code in ('��������', '����.���-��', '������� ������������'))
    inner join ins_department d on (f.deptrn = d.rn)
    left outer join clnpsdep ps on (f.psdeprn = ps.rn)
    left outer join clnposts pt on (f.postrn = pt.rn)
    --where f.begeng<=dEnd --and (f.endeng>=dBegin or f.endeng is null)
  ) loop
  
    dBeginMain := d_max2(i.mainbeg, dBegin);
    dEndMain := d_min2(i.mainend, dEnd);
    
    select sum(h.workedhours) into nFact
    from CLNPSPFMWD w, CLNPSPFMWH h, SL_HOURS_TYPES ht, SLDAYSTYPE dt
    where w.prn = i.main_rn
    and h.prn = w.rn and h.hourstype = ht.rn
    and w.WORKDATE >= dBegin
    and w.WORKDATE <= dEnd
    and w.daystype = dt.rn(+)
    and nvl(dt.absence_sign,0) = 0
    and ht.base_sign = 1;
    
    PKG_CALENDAR.GET_VALUE(24404,0,i.schedule,dBegin,dEnd,null,nNorma); 

    if nNorma>0 then
       nKoeffNumb := nFact/nNorma;
    else
       nKoeffNumb := 0;
    end if;
    
    if nKoeffNumb>1 then 
      nKoeffNumb := 1;
    end if;
    
    select sum(s.sum) into nSummMain 
    from slpays s, 
    (
      select distinct str.slcompcharges, str.formula
      from slcompgr gr, slcompgrstruct str, slcompcharges ch
      where str.prn = gr.rn
      and ch.compch_type = 10
      and str.slcompcharges = ch.rn
      and gr.crn in (
        select rn from acatalog c start with c.name = '�_���������������' 
        connect by prior c.rn = c.crn
      )
      and gr.code <> '��_��������'
    ) str
    where s.clnpspfm = i.main_rn
    and s.slcompcharges = str.slcompcharges
    and (str.FORMULA is null
        or
        (
         (
          (instr(str.FORMULA,'@') > 0 and s.YEARFOR = nYEAR and (s.MONTHFOR between nMONTHBEGIN and nMONTHEND))
          or
          (instr(str.FORMULA,'@') = 0 and s.YEAR = nYEAR and (s.MONTH between nMONTHBEGIN and nMONTHEND))
         )
         and
         (
          instr(str.FORMULA,'=') = 0 or s.YEARFOR = s.YEAR and s.MONTHFOR = s.MONTH
         )
        )
    );
    
    SELECT max(P.NAME) into sDOL
    FROM PGPOST P, PGNUNLOADSP S, PGNUNLOAD L
    WHERE P.RN = S.PROGNOZ_RN 
    AND S.PRN = L.RN
    and L.PARUS = 'ClientPosts'
    AND S.PARUS_RN = i.postrn;
    
    insert into temp_report_main (
    persrn, pfmrn_main, depname, postname, ratevol, wfact, wnorma,  summmain, mainbeg, mainend, isexternal
    )
    values (
    i.persrn, i.main_rn, i.depcode, nvl(sDOL,'(�����)'||i.psdep_name), nKoeffNumb, nFact, nNorma, nSummMain, i.mainbeg, i.mainend, i.isexternal
    );

  end loop;
  
  commit;
  
end;

-- ������ ����������
declare 
dBegin date;
dEnd date;
nYear number;
nMonthBegin number;
nMonthEnd number;
nKoeffNumb number(10,5);
nSummMain number(12,2); 
nSummComb number(12,2); 
dBeginMain date;
dEndMain date;
sDOL varchar2(250);
begin
  dBegin := to_date('01.01.2023','dd.mm.yyyy');
  dEnd := to_date('31.03.2023','dd.mm.yyyy');
  
  nYear := extract(year from dBegin);
  nMonthBegin := extract(month from dBegin);
  nMonthEnd := extract(month from dEnd);
  
  delete from temp_report_comb;
  commit;
  
  for i in (
    select f.persrn, f.rn as main_rn, d.code as depcode, 
    nvl(ps.postrn, pt.rn) as postrn, ps.psdep_name,
    f.begeng as mainbeg, f.endeng as mainend 
    from clnpspfm f
    inner join clnpspfmtypes t on (f.clnpspfmtypes = t.rn and t.code in ('����.���-��','�����.�������.','�����.����','�����.�������.','���������� ���-��','���������� ���������'))
    inner join ins_department d on (f.deptrn = d.rn)
    left outer join clnpsdep ps on (f.psdeprn = ps.rn)
    left outer join clnposts pt on (f.postrn = pt.rn)
    --where f.begeng<=dEnd --and (f.endeng>=dBegin or f.endeng is null)
  ) loop
  
    dBeginMain := d_max2(i.mainbeg, dBegin);
    dEndMain := d_min2(i.mainend, dEnd);

    nKoeffNumb := (dEndMain-dBeginMain+1)/(dEnd-dBegin+1);
    
    select sum(s.sum) into nSummMain 
    from slpays s, 
    (
      select distinct str.slcompcharges, str.formula
      from slcompgr gr, slcompgrstruct str, slcompcharges ch
      where str.prn = gr.rn
      and ch.compch_type = 10
      and str.slcompcharges = ch.rn
      and gr.crn in (
        select rn from acatalog c start with c.name = '�_���������������' 
        connect by prior c.rn = c.crn
      )
      and gr.code <> '��_��������'
    ) str
    where s.clnpspfm = i.main_rn
    and s.slcompcharges = str.slcompcharges
    and (str.FORMULA is null
        or
        (
         (
          (instr(str.FORMULA,'@') > 0 and s.YEARFOR = nYEAR and (s.MONTHFOR between nMONTHBEGIN and nMONTHEND))
          or
          (instr(str.FORMULA,'@') = 0 and s.YEAR = nYEAR and (s.MONTH between nMONTHBEGIN and nMONTHEND))
         )
         and
         (
          instr(str.FORMULA,'=') = 0 or s.YEARFOR = s.YEAR and s.MONTHFOR = s.MONTH
         )
        )
    );
    
    SELECT max(P.NAME) into sDOL
    FROM PGPOST P, PGNUNLOADSP S, PGNUNLOAD L
    WHERE P.RN = S.PROGNOZ_RN 
    AND S.PRN = L.RN
    and L.PARUS = 'ClientPosts'
    AND S.PARUS_RN = i.postrn;
    
    insert into temp_report_comb (
    persrn, pfmrn_comb, depname, postname, ratevol, summcomb, combbeg, combend
    )
    values (
    i.persrn, i.main_rn, i.depcode, nvl(sDOL,'(�����)'||i.psdep_name), nKoeffNumb, nSummMain, i.mainbeg, i.mainend
    );

  end loop;
  
  commit;
  
end;



-- �������� --

select sum(summmain) from temp_report_main r

select sum(summcomb) from temp_report_comb r

-- ����������
select * from temp_report_comb c
where not exists (
 select 1 from temp_report_main m
 where c.persrn = m.persrn
 and (c.combbeg <= m.mainend or m.mainend is null)
 and (c.combend >= m.mainbeg or c.combend is null)
)
and nvl(SUMMCOMB,0)<>0

-- ��������

select c.pfmrn_comb, 
m.mainbeg, m.mainend, 
c.combbeg, c.combend 
from temp_report_main m, temp_report_comb c
where m.persrn = c.persrn
and c.pfmrn_comb = 191473133

14-APR-15
01-JAN-23
31-DEC-22
01-JAN-23

-- ������������� --

declare
dBegin date;
dEnd date;
dBeginMain date;
dEndMain date;
dBeginComb date;
dEndComb date;
dBeginCombL date;
dEndCombL date;
nKoeffComb number(10,5);
begin

  delete from temp_comb_distrib;

  for i in (
    select 
    c.pfmrn_comb, m.pfmrn_main, m.persrn, c.summcomb,
    m.mainbeg, m.mainend, c.combbeg, c.combend 
    from temp_report_main m, temp_report_comb c
    where m.persrn = c.persrn
    --and m.persrn = 113049625
  ) 
  loop

    dBegin := to_date('01.01.2023','dd.mm.yyyy');
    dEnd := to_date('31.03.2023','dd.mm.yyyy');
  
    dBeginMain := d_max2(i.mainbeg, dBegin);
    dEndMain := d_min2(i.mainend, dEnd);

    dBeginComb := d_max2(i.combbeg, dBegin);
    dEndComb := d_min2(i.combend, dEnd);
    
    -- ���������� ������ � ������ �����������
    if (dEndComb>=dBeginMain) and (dBeginComb<=dEndMain) then
      
      dBeginCombL := d_max2(dBeginComb, dBeginMain);
      dEndCombL :=  d_min2(dEndComb, dEndMain);
      
      nKoeffComb := (dEndCombL-dBeginCombL+1)/(dEndComb-dBeginComb+1);
      
      /*
      dbms_output.put_line('---');
      dbms_output.put_line(i.pfmrn_comb);
      dbms_output.put_line(dBeginComb);
      dbms_output.put_line(dEndComb);
      dbms_output.put_line(dBeginMain);
      dbms_output.put_line(dEndMain);
      dbms_output.put_line(dBeginCombL);
      dbms_output.put_line(dEndCombL);
      dbms_output.put_line(nKoeffComb);
      dbms_output.put_line('===');
      */
      
      if nKoeffComb>0 then

         insert into temp_comb_distrib(persrn, pfmrn_main, pfmrn_comb, koeff, summcomb, combbeg, combend, mainbeg, mainend)
         values (i.persrn, i.pfmrn_main, i.pfmrn_comb, nKoeffComb, i.summcomb, i.combbeg, i.combend, i.mainbeg, i.mainend); 

        --update temp_report_comb set 
        --koeff = nKoeffComb,  
        --pfmrn_main = i.pfmrn_main
        --where pfmrn_comb = i.pfmrn_comb;
      end if;
    
    end if;
    
    commit;

  end loop;
end;

-- �������� �������� - ������ ������ k = 1
begin
  for i in (
    select s1.persrn, s1.pfmrn_comb, s1.pfmrn_main1, s1.summcomb, 
    s1.combbeg, s1.combend, m1.mainbeg, m1.mainend
    from
    (
      select c.*, 
      (
      select m.pfmrn_main from temp_report_main m 
      where m.persrn = c.persrn and nvl(m.summmain,0)<>0
      and m.mainbeg<=c.combbeg
      ) pfmrn_main1
      from temp_report_comb c
      where nvl(c.summcomb,0)<>0
      and not c.pfmrn_comb in (
      select d.pfmrn_comb from temp_comb_distrib d
      )
    ) s1, temp_report_main m1
    where s1.pfmrn_main1 = m1.pfmrn_main
 ) loop
 
     insert into temp_comb_distrib(persrn, pfmrn_main, pfmrn_comb, koeff, summcomb, combbeg, combend, mainbeg, mainend)
     values (i.persrn, i.pfmrn_main1, i.pfmrn_comb, 1, i.summcomb, i.combbeg, i.combend, i.mainbeg, i.mainend); 
 
 end loop;
 
 commit;
 
end;

-- ��������� ��������
select * from temp_report_comb c 
where nvl(c.summcomb,0)<>0
and not c.pfmrn_comb in (
select d.pfmrn_comb from temp_comb_distrib d
)

-- ��������� ����� �������������
select d.pfmrn_comb, sum(d.koeff) 
from temp_comb_distrib d
group by d.pfmrn_comb
having round(sum(d.koeff),2) <> 1

-- �������������� �����������
declare 
nKoeff number(10,5);
begin
  for i in (  
    select d.pfmrn_comb
    from temp_comb_distrib d
    group by d.pfmrn_comb
    having round(sum(d.koeff),2) <> 1
    order by 1
  ) loop
  
    nKoeff := 1;
    for j in (
      select rowid
      from temp_comb_distrib d
      where d.pfmrn_comb = i.pfmrn_comb
    ) loop
      update temp_comb_distrib set koeff = nKoeff where rowid = j.rowid;
      nKoeff := 0;      
    end loop;

  end loop;
end;

-- �������� ����� �����
select sum(d.koeff*d.summcomb) from temp_comb_distrib d; 
select sum(d.summcomb) from temp_report_comb d; 

select sum(d.koeff*d.summcomb) from temp_comb_distrib d; 

-- �������� ����������� � ������� ��������� ����������
begin
  for i in ( 
    select c.persrn, c.pfmrn_comb, c.summcomb, c.combbeg, c.combend, m.mainbeg, m.mainend, m.pfmrn_main
    from temp_report_comb c, temp_report_main m 
    where not exists (
    select d.pfmrn_comb from temp_comb_distrib d where d.pfmrn_comb = c.pfmrn_comb 
    )
    and nvl(c.SUMMCOMB,0)<>0
    and c.persrn = m.persrn
    and not exists (
      select 1 from temp_report_main m1
      where m.persrn = m1.persrn
      and m.mainbeg<m1.mainbeg
    )
  ) loop

     insert into temp_comb_distrib(persrn, pfmrn_main, pfmrn_comb, koeff, summcomb, combbeg, combend, mainbeg, mainend)
     values (i.persrn, i.pfmrn_main, i.pfmrn_comb, 1, i.summcomb, i.combbeg, i.combend, i.mainbeg, i.mainend); 

  end loop;
end;


select * from clnpspfm f where f.persrn = 194026962


select p.code, m.* from temp_report_main m, clnpersons p 
where m.persrn = p.rn
and m.postname = '(�����)�������� �� ������� ������'
--and DEPNAME = '�������� ���.'
and m.ratevol>0

198228832

select 0.33258+0.61455 from dual


-- ����� --!!!
-- �� ���� --
select m.postname, 
sum(greatest(0,m.ratevol)) as rate, 
sum((select sum(d.koeff*d.summcomb) from temp_comb_distrib d where d.pfmrn_main = m.pfmrn_main)) as sumcomb,
sum(m.summmain) as summain
from temp_report_main m
group by m.postname
having sum(m.summmain)<>0
order by 1;

-- ��� ������� --
select m.postname, 
sum(greatest(0,m.ratevol)) as rate, 
sum((select sum(d.koeff*d.summcomb) from temp_comb_distrib d where d.pfmrn_main = m.pfmrn_main)) as sumcomb,
sum(m.summmain) as summain
from temp_report_main m
where m.isexternal = 0
group by m.postname
having sum(m.summmain)<>0
order by 1;




select * from temp_comb_distrib
select * from temp_report_comb

select * from CLNPSPFMWD w
select * from CLNPSPFMWH

select * from v_clnpspfm

--- ������������ �����
select sum(h.workedhours) 
from CLNPSPFMWD w, CLNPSPFMWH h, SL_HOURS_TYPES ht, SLDAYSTYPE dt
where w.prn = 198228832
and h.prn = w.rn and h.hourstype = ht.rn
and w.WORKDATE >= to_date('01.03.2023','dd.mm.yyyy')
and w.WORKDATE <= to_date('31.03.2023','dd.mm.yyyy')
and w.daystype = dt.rn(+)
and nvl(dt.absence_sign,0) = 0
and ht.base_sign = 1;



select * from clnpspfmhs where prn = 198228832

declare
nNorma number(10,2);
begin
 PKG_CALENDAR.GET_VALUE(
    24404,
    0,
    288559, 
    to_date('01.03.2023','dd.mm.yyyy'),
    to_date('31.03.2023','dd.mm.yyyy'),
    null, 
    nNorma
 ); 
 dbms_output.put_line(nNorma);
end;


select m.postname, m.category, 
sum(nvl(n.numval,0)) as numval, 
sum(greatest(0,m.ratevol)) as rate,
sum((select sum(d.koeff*d.summcomb) from temp_comb_distrib d where d.pfmrn_main = m.pfmrn_main)) as sumcomb,
sum(m.summmain) as summain
from temp_report_main m
left outer join temp_report_num n on (n.pfmrn_main = m.pfmrn_main) 
where m.isexternal = 0
--and m.summmain<>0
group by m.postname, m.category
--having sum(m.summmain)<>0
order by 1

select sum(summmain) from temp_report_main 

-- ��� ������� --
select m.postname, 
sum(greatest(0,m.ratevol)) as rate, 
sum((select sum(d.koeff*d.summcomb) from temp_comb_distrib d where d.pfmrn_main = m.pfmrn_main)) as sumcomb,
sum(m.summmain) as summain
from temp_report_main m
where m.isexternal = 0
group by m.postname
having sum(m.summmain)<>0
order by 1;


