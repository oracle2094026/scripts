select sum(ct1.summ) from temp_report_costs ct1

select count(*) from temp_report_costs ct1

select count(distinct ct1.pfmrn) from temp_report_costs ct1

select ct1.pfmrn, count(*) from temp_report_costs ct1 group by ct1.pfmrn having count(*)>1

select sum(t.summain) from temp_report_test t 
where not exists (
 select 1 from temp_report_test t1
 where t1.pfmrn_main = t.pfmrn_main
 and (nvl(t1.costgrp_main,' ') = nvl(t.costgrp_main, ' '))
 and t1.rn>t.rn 
)


select sum(t.SUMM) from temp_report_test t 

select sum(t.SUMDISTR) from temp_report_test t 

select sum(t.summain) from temp_report_test t 

select * from temp_report_test t 

where not exists (
 select 1 from temp_report_test t1
 where t1.pfmrn_main = t.pfmrn_main
 and (nvl(t1.costgrp,' ') = nvl(t.costgrp, ' '))
 and t1.rn>t.rn 
)


select sum(t.summcomb) from temp_report_test t 

select * from temp_report_test t 
where exists (
 select 1 from temp_report_test t1
 where t1.pfmrn_main = t.pfmrn_main
 and (nvl(t1.costgrp,' ') = nvl(t.costgrp, ' '))
 and t1.rn>t.rn 
)
order by AGNABBR, rn

select * from temp_report_test t where t.pfmrn_main = 212667095




select * from temp_comb_distrib d

temp_report_main m 

--drop table temp_report_test

select * 
from temp_report_comb c, temp_report_costs ct2, temp_comb_distrib d

select ct2.pfmrn, ct2.costgrp, c.depname, c.postname, ct2.summ*d.koeff as sumdistr
from temp_report_comb c, temp_report_costs ct2, temp_comb_distrib d 
where c.pfmrn_comb = ct2.pfmrn and d.pfmrn_comb = c.pfmrn_comb


select sum(ct2.summ*d.koeff)
from temp_report_comb c, temp_report_costs ct2, temp_comb_distrib d 
where c.pfmrn_comb = ct2.pfmrn and d.pfmrn_comb = c.pfmrn_comb

drop table temp_report_test

create table temp_report_test as
select rownum as rn, m1.pfmrn as pfmrn_main, m1.costgrp as costgrp_main, m1.depname as depname_main, m1.category, m1.postname as postname_main, m1.summain,
c1.pfmrn as pfmrn_comb, c1.costgrp as costgrp_comb, c1.depname as depname_comb, c1.postname as postname_comb, c1.sumdistr
from (
 select ct1.pfmrn, ct1.costgrp, m.depname, m.category, m.postname, ct1.summ as summain 
 from temp_report_main m, temp_report_costs ct1, clnpspfm fm, clnpersons p, agnlist a
 where m.pfmrn_main = ct1.pfmrn and m.isexternal = 0
 and m.pfmrn_main = fm.rn and m.persrn = p.rn and p.pers_agent = a.rn 
) m1
full join
(
  select d.pfmrn_main, ct2.pfmrn, ct2.costgrp, c.depname, c.postname, ct2.summ*d.koeff as sumdistr
  from temp_report_comb c, temp_report_costs ct2, temp_comb_distrib d 
  where c.pfmrn_comb = ct2.pfmrn and d.pfmrn_comb = c.pfmrn_comb
) c1 on (m1.pfmrn = c1.pfmrn_main and c1.costgrp = m1.costgrp)
order by m1.pfmrn, c1.pfmrn_main

select * from temp_report_main where agnabbr = '������� �. �./�'
select * from v_clnpspfm where nrn = 210064397
select * from temp_comb_distrib where agnabbr is null
select * from temp_report_main where agnabbr is null

select * from temp_report_costs where costgrp is null

select * from 



select m1.depname, m1.category, m1.agnabbr, m1.pfmrn_main, m1.costgrp, c1.costgrp,
(select ptm.code from clnposts ptm where ptm.rn = m1.postrn) as postcode_m,
(select ptc.code from clnposts ptc where ptc.rn = c1.postrn) as postcode_c,
(select pdm.psdep_code from clnpsdep pdm where pdm.rn = m1.psdeprn) as pspost_m,
(select pdc.psdep_code from clnpsdep pdc where pdc.rn = c1.psdeprn) as pspost_c,
(select oc.code from officercls oc where oc.rn = c1.officercls) as category_c,
m1.postname, m1.mainbeg, m1.mainend, 
c1.depname as depname_c, c1.combbeg, c1.combend, 
c1.koeff, 
m1.summmain, 
c1.summcomb, 
c1.sumdistr
from (
 select m.pfmrn_main, m.depname, m.category, 
 a.agnabbr, fm.postrn, fm.psdeprn, m.postname, 
 m.mainbeg, m.mainend, ct1.costgrp, ct1.summ as summmain
 from temp_report_main m, temp_report_costs ct1, 
 clnpspfm fm, clnpersons p, agnlist a
 where m.pfmrn_main = ct1.pfmrn and m.isexternal = 0
 and m.pfmrn_main = fm.rn and m.persrn = p.rn 
 and p.pers_agent = a.rn 
) m1
full join
(
  select d.pfmrn_main, c.pfmrn_comb, c.depname, c.combbeg, c.combend, 
  fc.postrn, fc.psdeprn, fc.officercls, 
  d.koeff, ct2.summ as summcomb, ct2.costgrp, ct2.summ*d.koeff as sumdistr
  from temp_report_comb c, temp_report_costs ct2, 
  temp_comb_distrib d, clnpspfm fc 
  where c.pfmrn_comb = ct2.pfmrn 
  and d.pfmrn_comb = c.pfmrn_comb 
  and c.pfmrn_comb = fc.rn
) c1 on (m1.pfmrn_main = c1.pfmrn_main and c1.costgrp = m1.costgrp)
order by 1,2,3,4,5,6


select * from temp_comb_distrib


select sum(ct1.summ) 

select m.pfmrn_main, max(m.summmain), sum(ct1.summ) 
from temp_report_main m, temp_report_costs ct1 
where m.pfmrn_main = ct1.pfmrn
group by m.pfmrn_main
having max(m.summmain)<>sum(ct1.summ)


select sum(m.summmain) from temp_report_main m 


select sum(ct1.summ)
from temp_report_main m, temp_report_costs ct1 
where m.pfmrn_main = ct1.pfmrn
and m.isexternal = 0


select m.pfmrn_main, count(*) from temp_report_main m group by m.pfmrn_main having count(*)>1

select * from temp_report_test


create table temp_report_test as
select rownum as rn, m.depname, m.category, a.agnabbr, m.pfmrn_main, ct1.costgrp,
(select ptm.code from clnposts ptm where ptm.rn = fm.postrn) as postcode_m,
(select ptc.code from clnposts ptc where ptc.rn = fc.postrn) as postcode_c,
(select pdm.psdep_code from clnpsdep pdm where pdm.rn = fm.psdeprn) as pspost_m,
(select pdc.psdep_code from clnpsdep pdc where pdc.rn = fc.psdeprn) as pspost_c,
(select oc.code from officercls oc where oc.rn = fc.officercls) as category_c,
m.postname,
m.mainbeg, m.mainend, 
c.depname as depname_c,
c.combbeg, c.combend, 
n.numval, 
d.koeff, 
n.postname as postname_n, 
ct1.summ as summmain, 
ct2.summ as summcomb, 
round(ct2.summ*d.koeff, 2) as sumdistr
from temp_report_main m 
left outer join temp_report_costs ct1 on (m.pfmrn_main = ct1.pfmrn)
left outer join temp_comb_distrib d on (d.pfmrn_main = m.pfmrn_main)
left outer join temp_report_comb c on (d.pfmrn_comb = c.pfmrn_comb)
left outer join temp_report_costs ct2 on (c.pfmrn_comb = ct2.pfmrn and ct1.costgrp = ct2.costgrp)
left outer join clnpspfm fm on (m.pfmrn_main = fm.rn)
left outer join clnpspfm fc on (c.pfmrn_comb = fc.rn)
left outer join clnpersons p on (m.persrn = p.rn)
left outer join agnlist a on (p.pers_agent = a.rn)
where m.isexternal = 0
order by 1,2,3,4
