-- 0) ��� ���������
update FRMRPFMDATA set reason = '';

delete from FRMRPARUSDATA;
commit;

declare
nclnpspfm number; 
begin
    for i in (
        
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and s2.company = s1.company
        
    )
    loop
      -- ���� ����������
          
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      and fd.deptrn = f.deptrn 
      and fd.depid = i.deptid 
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn 
      and s1.company = f.company
      and s1.postid = i.postid
      and s1.postrn = ps.postrn      
      and f.begeng = i.begindate
      and nvl(f.endeng,to_date('01.01.2060','dd.mm.yyyy')) = nvl(i.enddate,to_date('01.01.2060','dd.mm.yyyy'));
          
      if not nclnpspfm is null then
        insert into FRMRPARUSDATA (RN, PRN, CLNPSPFM) values (gen_id(), i.rn, nclnpspfm);
        commit;
      end if;

    end loop;

end;


update FRMRPFMDATA set REASON = '���' where not exists (select 1 from FRMRPARUSDATA pd where pd.prn = FRMRPFMDATA.rn);

-- ���������� FRMRPARUSDATA � ������� ���������
--1) ��������� ���������, ���� ������, ��� ���������� (�����), �� �� ��������� ���� ���������.

declare
nclnpspfm number; 
begin
  for i in (
    
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and s2.company = s1.company
          and f.reason like '%���%'          
    
    )
    loop
      -- ���� ����������
      
      --1) ��������� ���������, ���� ������, ��� ���������� (�����), �� �� ��������� ���� ���������.
      
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      and fd.deptrn = f.deptrn 
      and fd.depid = i.deptid 
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn 
      and s1.company = f.company
      and s1.postid = i.postid
      and s1.postrn = ps.postrn      
      and f.begeng = i.begindate
      and nvl(f.endeng,to_date('01.01.2060','dd.mm.yyyy')) <> nvl(i.enddate,to_date('01.01.2060','dd.mm.yyyy'));
      
      if not nclnpspfm is null then
        insert into FRMRPARUSDATA (RN, PRN, CLNPSPFM) select gen_id(), i.rn, nclnpspfm from dual 
        where not exists (select 1 from FRMRPARUSDATA pd where pd.CLNPSPFM = nclnpspfm);
        if SQL%ROWCOUNT>0 then
          update FRMRPFMDATA set reason = reason||',���' where rn = i.rn;
        end if;
        commit;
      end if;

    end loop;
end;


-- ���������� FRMRPARUSDATA � ������� ���������
-- 2.2) ��������� ��� ���������� (��������), ���������, ���� ���������, �� �� ��������� ���� ������. ������ ����������� ������� ���� � ������ ����������� ��� ������.

declare
nclnpspfm number; 
begin
  for i in (
    
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and f.posttypeid = 1
          and s2.company = s1.company
          and f.reason like '%���%'
    
    )
    loop
      -- ���� ����������
      
      -- 2) ��������� ��� ���������� (��������), ���������, �� �� ��������� ���� ������. ������ ����������� ������� ���� � ������ ����������� ��� ������.
      
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      and fd.deptrn = f.deptrn 
      and fd.depid = i.deptid 
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn 
      and s1.company = f.company
      and s1.postid = i.postid
      and s1.postrn = ps.postrn      
      and f.begeng <> i.begindate;
      --and nvl(f.endeng,to_date('01.01.2060','dd.mm.yyyy')) = nvl(i.enddate,to_date('01.01.2060','dd.mm.yyyy'));
      
      if not nclnpspfm is null then
        insert into FRMRPARUSDATA (RN, PRN, CLNPSPFM) select gen_id(), i.rn, nclnpspfm from dual 
        where not exists (select 1 from FRMRPARUSDATA pd where pd.CLNPSPFM = nclnpspfm);
        if SQL%ROWCOUNT>0 then
          update FRMRPFMDATA set reason = reason||',���' where rn = i.rn;
        end if;
        commit;
      end if;

    end loop;
end;


-- 2.1) ��������� ��� ���������� (��������), ���� ������, �� �� ���������.

declare
nclnpspfm number; 
begin
  for i in (
    
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and f.posttypeid = 1
          and s2.company = s1.company
          and f.reason like '%���%'
          and not f.reason like '%���%'
          --and not f.reason like '%���%'
    
    )
    loop
      -- ���� ����������
      
      -- 2.1) ��������� ��� ���������� (��������), ���� ������, �� �� ���������.
      
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      and fd.deptrn = f.deptrn 
      and fd.depid = i.deptid 
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn
      and s1.company = f.company
      and s1.postid = i.postid
      and s1.postrn <> ps.postrn -- �� ���������     
      and f.begeng = i.begindate;
      --and nvl(f.endeng,to_date('01.01.2060','dd.mm.yyyy')) = nvl(i.enddate,to_date('01.01.2060','dd.mm.yyyy'));
      
      if not nclnpspfm is null then
        insert into FRMRPARUSDATA (RN, PRN, CLNPSPFM) values (gen_id(), i.rn, nclnpspfm);
        update  FRMRPFMDATA set reason = reason||',���' where rn = i.rn;
        commit;
      end if;

    end loop;
end;


-- 2.3) ��������� ��� ���������� (��������), ���� ������, �� �� �������������.

declare
nclnpspfm number; 
begin
  for i in (
    
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and f.posttypeid = 1
          and s2.company = s1.company
          and f.reason like '%���%'
          and not f.reason like '%���%'
          --and not f.reason like '%���%'
    
    )
    loop
      -- ���� ����������
      
      -- 2.1) ��������� ��� ���������� (��������), ���� ������, �� �� ���������.
      
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      and fd.depid = i.deptid 
      and fd.deptrn <> f.deptrn -- �� ���������
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn
      and s1.company = f.company
      and s1.postid = i.postid
      and s1.postrn = ps.postrn      
      and f.begeng = i.begindate;
      --and nvl(f.endeng,to_date('01.01.2060','dd.mm.yyyy')) = nvl(i.enddate,to_date('01.01.2060','dd.mm.yyyy'));
      
      if not nclnpspfm is null then
        insert into FRMRPARUSDATA (RN, PRN, CLNPSPFM) select gen_id(), i.rn, nclnpspfm from dual 
        where not exists (select 1 from FRMRPARUSDATA pd where pd.CLNPSPFM = nclnpspfm);
        if SQL%ROWCOUNT>0 then
          update FRMRPFMDATA set reason = reason||',���' where rn = i.rn;
        end if;
        commit;
      end if;

    end loop;
end;

-- ���������� FRMRPARUSDATA � ������� ���������
-- 3) ��������� ��� ���������� (��������). ������ ������ �� ���������.

--update FRMRPFMDATA set reason = replace(reason, ',���','') where reason like '%���%';

--update FRMRPFMDATA set reason = replace(reason, ',�����,�����',',�����') where reason like '%���%';

declare
nclnpspfm number; 
begin
  for i in (
    
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and f.posttypeid = 1
          and s2.company = s1.company
          and f.reason like '%���%'
          and not f.reason like '%���%'
          and not f.reason like '%���%'
          and not f.reason like '%���%'
          and not f.reason like '%���%'
    
    )
    loop
      -- ���� ����������
      
      -- 3) ��������� ��� ���������� (��������). ������ ������ �� ���������.
      
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      --and fd.deptrn = f.deptrn 
      and fd.depid = i.deptid 
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn 
      and s1.company = f.company
      and s1.postid = i.postid;
      --and s1.postrn <> ps.postrn      
      --and f.begeng <> i.begindate
      --and nvl(f.endeng,to_date('01.01.2060','dd.mm.yyyy')) <> nvl(i.enddate,to_date('01.01.2060','dd.mm.yyyy'));
      
      if not nclnpspfm is null then
        insert into FRMRPARUSDATA (RN, PRN, CLNPSPFM) select gen_id(), i.rn, nclnpspfm from dual 
        where not exists (select 1 from FRMRPARUSDATA pd where pd.CLNPSPFM = nclnpspfm);
        if SQL%ROWCOUNT>0 then
          update FRMRPFMDATA set reason = reason||',���' where rn = i.rn;
        end if;
        commit;
      end if;

    end loop;
end;


--- �� ��������� ���� ������, �� ���� ��������� �������

-- ���������� FRMRPARUSDATA � ������� ���������
-- ��������� ��� ���������� (��������), ���������, ���� ���������, �� �� ��������� ���� ������. ������ ����������� ������� ���� � ������ ����������� ��� ������.

declare
nclnpspfm number; 
begin
  for i in (
    
          select f.*, p.rn as persrn, s1.company as company1, s2.rn as nclnpfmtype
          from FRMRPFMDATA f,  clnpersons p, agnlist a, 
          (select distinct depid, company from frmr_depsid_khv) s1,
              
          (
            select t.rn, PT.ID, t.company from PGNUNLOADSP SP, PGPOSTTP PT, clnpspfmtypes t
            where sp.company = t.company 
            and sp.prognoz_rn = PT.rn
            and t.rn = sp.parus_rn
            and sp.PRN in (select l.nrn from V_PGNUNLOAD l where l.SPARUS_NAME = '���� ������������ ����������' and l.ncompany = t.company)
          ) s2
              
          where s1.depid = f.deptid
          and p.company = s1.company
          and p.pers_agent = a.rn
          and replace(replace(a.pension_nbr,' ',''),'-','') = f.snils
          and s2.ID = f.posttypeid
          and f.posttypeid = 1
          and s2.company = s1.company
          and f.reason like '%���%'
          and f.reason like '%���%'
    
    )
    loop
      -- ���� ����������
      
      -- 2) ��������� ��� ���������� (��������), ���������, �� �� ��������� ���� ������. ������ ����������� ������� ���� � ������ ����������� ��� ������.
      
      select max(f.rn) into nclnpspfm 
      from clnpspfm f, clnpsdep ps, frmr_depsid_khv fd, 
      (
        select p1.company, p1.rn as postrn, PT1.ID as postid
        from clnposts p1, PGNUNLOAD pl1, PGNUNLOADSP sp1, PGPOST PT1
        where pl1.Prognoz = 'PrognozPosts'
        and sp1.prn = pl1.rn
        and PT1.RN = sp1.prognoz_rn
        and p1.rn = sp1.parus_rn
        and p1.company = pl1.company
      ) s1
      where ps.rn = f.psdeprn 
      and f.persrn = i.persrn
      and fd.company = f.company
      and fd.deptrn = f.deptrn 
      and fd.depid = i.deptid 
      and f.clnpspfmtypes = i.nclnpfmtype 
      --and ps.postrn = i.parus_post_rn 
      and s1.company = f.company
      and s1.postid = i.postid
      and s1.postrn = ps.postrn      
      and f.begeng <> i.begindate
      and f.endeng is null 
      and i.enddate is null;
      
      if not nclnpspfm is null then

        update FRMRPFMDATA set reason = reason||',��������' where rn = i.rn;
        commit;
        
      end if;

    end loop;
end;

-- �����
--1) ������ �������
update frmrpfmdata set reason = reason||',�����' where rn in
(
  select fd.rn from frmrpfmdata fd where reason like '%���%' and not enddate is null
  --and exists (select 1 from frmrparusdata pd where pd.prn = fd.rn)
  and not exists (
  select 1 from clnpspfm f, clnpersons p, agnlist a where f.persrn = p.rn and p.pers_agent = a.rn
  and replace(replace(a.pension_nbr,' ',''),'-','') = fd.snils
  and f.endeng is null
  )
) and not reason like '%�����%'


--2) ������ �������
update frmrpfmdata set reason = reason||',�����' where rn in
(
  select fd.rn from frmrpfmdata fd where reason like '%���%' and not reason like '%�����%' and not enddate is null
  and exists (select 1 from frmrparusdata pd, clnpspfm f where pd.prn = fd.rn and f.rn = pd.clnpspfm and not f.endeng is null)
)


--3) ������ �������
update frmrpfmdata set reason = reason||',�����' where rn in
(
  select fd.rn from frmrpfmdata fd where reason like '%���%' and not reason like '%�����%' and not enddate is null
  and exists (select 1 from frmrparusdata pd, clnpspfm f where pd.prn = fd.rn and f.rn = pd.clnpspfm and not f.endeng is null)
)

--4) ��������� �������
update frmrpfmdata set reason = reason||',�����' where rn in
(
  select fd.rn from frmrpfmdata fd where reason like '%���%' and not enddate is null
  and enddate < to_date('01.01.2019','dd.mm.yyyy') and not reason like '%�����%'
) 
