create or replace procedure P_INSERT_SLPAYS_KHV(sCharge varchar2, sCalctype varchar2)
as
  nRootCRN number(17);
  nCRN number(17);
  nRN number(17);
  nGrpRn number(17);
  nGrndRn number(17);
  nCompany number(17);
  nCompChType number(2);
  nDayStart number(2); 
  nDayEnd number(2); 
  dDateBegin date;
  dDateEnd date;
  nMonth number(2);  
  nYear number(4);
  nCharge number(17);
  nCalctype number(17);
  nCalculat number(17);
  nRow number;
begin

   nCompany := 24404;
   
   -- ��� ��� ����� ������ ��� �������� �������
   PKG_SLCALCWAGES.CHECK_PARAMS(nCalctype, nCalculat, nMonth,nYear);
   
   dDateBegin := to_date('01.'||to_char(nMonth,'99')||to_char(nYEAR,'9999'), 'dd.mm.yyyy');
   
   nDayStart := 1;
   dDateEnd := last_day(dDateBegin);
   nDayEnd := extract(day from dDateEnd);
   
   select max(rn) into nCharge from slcompcharges ch where ch.code = sCharge;
   if nCharge is null then
      p_exception(0, '�� ��������� ��� ���������� '||sCharge);
      return;
   end if;

   select max(rn) into nCalctype from slcalctype ct where ct.code = sCalctype;
   if nCalctype is null then
      p_exception(0, '�� ��������� ��� ������� '||sCalctype);
      return;
   end if;
   
   select max(rn) into nCalculat from slcalculat ct 
   where ct.slcalctype = nCalctype and ct.year = nYear and ct.monthnumb = nMonth;
   if nCalculat is null then
      p_exception(0, '�� ������ ��� ������� '||sCalctype||' � ������� �������.');
      return;
   end if;
   
   nRow := 0;
   for i in (
      select sht.*, f.crn, f.persrn, p.pers_agent
      from temp_ot1_sht sht, clnpspfm f, clnpersons p
      where sht.perform = f.rn and f.persrn = p.rn 
      and sht.sumot>0
      order by sht.ordnumb, sht.full_name
     )
   loop

       -- ���� ���������� ���������
       select max(rn) into nGrndRn from slpaygrnd g
       where g.slcompcharges = nCharge
       and g.prn = i.perform
       and g.bgndate<dDateEnd
       and (g.enddate>dDateBegin or g.enddate is null);
       
       if nGrndRn is null then

         nGrndRn := gen_id();
         
         insert into slpaygrnd (
           rn, prn, company, crn, slcompcharges, bgndate, enddate
         )  
         values (
           nGrndRn, i.perform, nCompany, i.crn, nCharge, dDateBegin, null
         ); 
         
       end if;
       
       -- ��������� ������� ����� �������
       select max(rn) into nRN from slpays p
       where p.slcompcharges = nCharge
       and p.clnpspfm = i.perform
       and p.year = nYear
       and p.Month = nMonth;
       
       if not nRn is null then
          p_exception(0, '� ������� ������� ��� ���� ������� '||sCharge||'. �������� ����� ����������. �������������� ���������� ������� �������.');
          return;
       end if;

       nGrpRn := gen_id();
       
       insert into slpaysgrp (RN, COMPANY, CRN, SLPAYGRND, SLCALCULAT, SLCOMPCHARGES, CLNPSPFM, CLNPERSONS, AGENT, YEAR, MONTH)
       values (nGrpRn, nCompany, i.crn, nGrndRn, nCalculat, nCharge, i.perform, i.persrn, i.pers_agent, nYear, nMonth) ;

       nRN := gen_id();

       insert into slpays (
          RN, COMPANY, CRN, SLPAYGRND, SLCALCULAT, SLPAYSGRP, SLCOMPCHARGES, CLNPSPFM, CLNPERSONS, AGENT, SLCALCTYPE,
          YEAR, MONTH, YEARFOR, MONTHFOR, BGNFOR, ENDFOR, LOCKED, REVERSE, SUM
       )
       values (
         nRN, nCompany, i.crn, nGrndRn, nCalculat, nGrpRn, nCharge, i.perform, i.persrn, i.persrn, nCalctype,
         nYear, nMonth, nYear, nMonth,
         nDayStart, nDayEnd, 1, 0, i.sumot
       );
       
       nRow := nRow+1;
       
   end loop;
   
   if nRow = 0 then
     p_exception(0, '������ �� ��������. �������� - ������������ ����.');
   end if;

   commit;
   
end;
/
