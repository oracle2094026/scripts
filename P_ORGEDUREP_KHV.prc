CREATE OR REPLACE PROCEDURE "P_ORGEDUREP_KHV" (sORGs varchar2, sCateg varchar2, sPost varchar2, sspec varchar2, nOrg number, nTarget number)
as
 iLine number;
 sOrgss varchar2(2000);
begin

  if (sORGs is null and not nOrg is null) then
    select name into sOrgss from Companies where rn = nOrg;
  else
    sOrgss :=  sORGs;
  end if;

/* ������ */
  PRSG_EXCEL.PREPARE;

  /* ��������� �������� �������� ����� */
  PRSG_EXCEL.SHEET_SELECT( '������'  );

  /* �������� */

  PRSG_EXCEL.LINE_DESCRIBE( '������' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'NUM' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'COMPANY' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'FIO' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SNILS' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DR' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SPODR' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SDOL' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SSTA' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'STA' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'PODR' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DOL' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DNACH' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DKON' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'VIDISP' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'CATEG' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'PRIM' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'EDUC' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SERTIF' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'ACCRED' );

  for i in (

      select * from
      (
            select
            (select name from companies c where c.rn = p.company) as company_name,
            al.agnfamilyname||' '||al.agnfirstname||' '||al.agnlastname as agnname,
            nvl(al.pension_nbr,'-') as snils,
            to_char(al.agnburn,'dd.mm.yyyy') as agnburn,
            (select d.name from CLNPSDEP dp, ins_department D where f.psdeprn = dp.rn and dp.deptrn = d.rn) as staff_dep,
            (select PSDEP_NAME from CLNPSDEP dp where f.psdeprn = dp.rn) as staff_post,
            (select dphs.rateacc from CLNPSDEPHS dphs where f.psdeprn = dphs.prn and dphs.do_act_from = (  select max(do_act_from) from CLNPSDEPHS dphs1 where dphs1.prn = f.psdeprn )  ) as staff_post_rate,
            (select fh.rateacc from  CLNPSPFMHS fh where fh.prn = f.rn and fh.do_act_from = (select max(fh1.do_act_from) from CLNPSPFMHS fh1 where fh1.prn = f.rn)) as pfm_rate,
            (select name from ins_department d where d.rn = f.deptrn) as dep_name,
            (select name from clnposts ps where ps.rn = f.postrn) as post_name,
            f.begeng,
            f.endeng,
            t.name as type_name,
            o.code as type_code,
            f.note,

            (select to_char(sys_xmlagg(xmlelement(col,

            (select t.Name from PREDTYPE t where rn = edu.educ_vid)||', '||
            '������:'||to_char(edu.begin_date,'dd.mm.yyyy')||'-'||to_char(edu.end_date,'dd.mm.yyyy')||', '||
            '��������: �'|| edu.doc_numb||', �����: '||edu.doc_ser||' ����� '||to_char(edu.doc_when,'dd.mm.yyyy') ||', '||
            '���:'||(select t.Code from EDINSTITUT t where rn = edu.educ_inst)||', '||
            '�������������:'||(select t.Name from PREDSPEC t where rn = edu.educ_spec)||', '||
            '������������:'||(select t.Name from PRDPLQLF t where rn = edu.prdplqlf)||', '||
            '����:'||(select t.name from PRCOURSE t where t.rn = edu.prcourse)||' '||chr(10)||chr(13)

            )).extract('/ROWSET/COL/text()').getclobval()) from agneduc edu where edu.prn = al.rn and (edu.target_set=1 or nvl(nTarget,0)=0)) as educ,

            (select to_char(sys_xmlagg(xmlelement(col,

            '����:  '||to_char(crt.certif_date,'dd.mm.yyyy')||',  �������������: '||(select name from PRPROF p where p.rn = crt.prprof)||', '||
            '��������: �'|| crt.docnumb||', �����: '||crt.sernumb||' ����� '||to_char(crt.docdate,'dd.mm.yyyy') ||', '||
            '���:'||(select t.Code from EDINSTITUT t where rn = crt.edinstitut)||' '||chr(10)||chr(13)

            )).extract('/ROWSET/COL/text()').getclobval()) from CLNPERSCERTIF crt where crt.prn = p.rn) as sertif, 


            (select to_char(sys_xmlagg(xmlelement(col,

            '���: '||acc.spraccrknd||', ���� ������:  '||to_char(acc.dbegin_date,'dd.mm.yyyy')||', ���� ���������:  '||to_char(acc.dend_date,'dd.mm.yyyy')||
            ',  �������������: '||acc.seduc_spec||', '||
            '��������: �'||trim(acc.sdocnumb)||', �����: '||trim(acc.ssernumb)||' ����� '||to_char(acc.ddocdate,'dd.mm.yyyy') ||', '||
            '���:'||acc.saccr_place||' '||chr(10)||chr(13)

            )).extract('/ROWSET/COL/text()').getclobval()) from V_CLNPERSACCR acc where acc.nprn = p.rn) as accred

            from CLNPERSONS p, agnlist al, CLNPSPFM f, clnpspfmtypes t, officercls o

            where p.pers_agent = al.rn
            and f.persrn = p.rn
            and t.rn = f.clnpspfmtypes
            and o.rn = f.officercls
            and (sCateg is null or lower(o.name) like '%'||lower(sCateg)||'%')
            and (sOrgss is null or p.company in (select rn from companies where instr(sOrgss,name)>0))
            and f.endeng is null
            and t.is_primary = 1

            and ((
              exists (select 1 from agneduc ad where ad.target_set=1 and ad.prn = al.rn)
            ) or  nvl(nTarget,0)=0 )

            and ((
              exists (select 1 from agneduc ad where ad.educ_spec in (select rn from PREDSPEC where lower(code) like '%'||sspec||'%' or lower(name) like '%'||sspec||'%' ) and ad.prn = al.rn)
              or
              exists (select 1 from CLNPERSCERTIF cf where cf.prprof in (select rn from PRPROF where lower(code) like '%'||sspec||'%' or lower(name) like '%'||sspec||'%' ) and cf.prn = p.rn)
            ) or sspec is null)
      ) s1  where (sPost is null or lower(nvl(staff_post, post_name)) like '%'||lower(sPost)||'%')
      order by s1.staff_dep, s1.agnname, s1.type_name

      ) loop

            iLine := PRSG_EXCEL.LINE_APPEND('������');
            PRSG_EXCEL.CELL_VALUE_WRITE( 'NUM', 0, iLine, iLine);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'COMPANY' , 0, iLine, i.company_name);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'FIO' , 0, iLine, i.agnname);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SNILS' , 0, iLine, i.snils);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DR' , 0, iLine, i.agnburn);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'SPODR' , 0, iLine, i.staff_dep);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SDOL' , 0, iLine, i.staff_post);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SSTA' , 0, iLine, i.staff_post_rate);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'STA' , 0, iLine, i.pfm_rate);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'PODR' , 0, iLine, i.dep_name);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DOL' , 0, iLine, i.post_name);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DNACH' , 0, iLine, i.begeng);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DKON' , 0, iLine, i.endeng);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'VIDISP' , 0, iLine, i.type_name);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'CATEG' , 0, iLine, i.type_code);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'PRIM' , 0, iLine, i.note);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'EDUC' , 0, iLine, i.educ);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SERTIF' , 0, iLine, i.sertif);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'ACCRED' , 0, iLine, i.accred);

    end loop;

    PRSG_EXCEL.LINE_DELETE('������',0);

end;
/
