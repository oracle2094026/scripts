create or replace procedure P_PAYSLIP_XML
(
  nCOMPANY        in number,            -- �����������
  nIDENT          in number,            -- ���������� ������
  sPostGs         in varchar2,
  sRankGs         in varchar2,
  sChargeMain     in varchar2,
  nOUTIDENT       out number
)
as
  dSCALC date;
  nMSCALC number(2);
  nYSCALC number(4);
  nRankGs number(17);
  nPostGs number(17);
  sRankCaption varchar2(250);
  sPostCaption varchar2(250);
  dBegin date;
  dEnd date;
  nPRC SLPAYGRNDPRM.NUM_VALUE%type;
  nWRK SLPAYSPRM.NUM_VALUE%type;
  nFOVTYPE number( 1 );
  sFileName varchar2(100);
  nPropERCCode number(17);
  nChargeMain number(17);

  sNomer varchar2(50);
  sHash raw(250);
  sHashV varchar2(250);
  
  function Num2Str(n number)
  return varchar2
  as
  begin
    if n is null then 
      return '0.00';
   else
      return trim(to_char(n, '99999990.00'));
   end if;   
  end;
  
begin

  dSCALC := trunc(GET_OPTIONS_DATE('SalaryCalcPeriod', nCOMPANY),'MONTH');

  find_slcompcharges_code(0,0,nCompany, sChargeMain, nChargeMain);
  FIND_GRSALARY_CODE(0,0, nCompany, sRankGs, nRankGs);
  FIND_GRSALARY_CODE(0,0, nCompany, sPostGs, nPostGs);
  find_docs_props_code(0, nCompany, '������', nPropERCCode);
  
  select gs.name into sRankCaption from GRSALARY gs where gs.rn = nRankGs; 
  select gs.name into sPostCaption from GRSALARY gs where gs.rn = nPostGs; 

  if not dSCALC is null then
    nMSCALC := D_MONTH(dSCALC);
    nYSCALC := D_YEAR(dSCALC);
  end if;

  nOUTIDENT := GEN_ID();

  for i in (
    select '544'||substr('000000000'||trim(f.numb),-9) as tabnum, f.rn, a.agnburn, pr.pers_note, r.name as srank,
    (
      select sr.rate 
      from slrankrate sr 
      where sr.prn = r.rn 
      and sr.slrankrate_begin<=dSCALC 
       and not exists (
           select 1 from slrankrate sr1
           where sr1.prn = sr.prn 
           and sr1.slrankrate_begin<=dSCALC
           and sr1.slrankrate_begin>sr.slrankrate_begin
       )
    ) as srankrate, 
    (
      select sum(p.sum) 
      from slpays p, slcompcharges ch 
      where p.clnpspfm = f.rn 
      and p.slcompcharges = ch.rn 
      and ch.compch_type = 10 
      and ch.confpay_sign = 0 
      and year = nYSCALC 
      and month = nMSCALC
      and not f_docs_props_get_str_value(nPropERCCode, 'SalaryCompensationsCharges', ch.rn) is null
    ) as SumCharge,
    (
      select sum(p.sum) 
      from slpays p, slcompcharges ch 
      where p.clnpspfm = f.rn 
      and p.slcompcharges = ch.rn 
      and ch.compch_type = 30 
      and ch.enum_type = 0 
      and year = nYSCALC 
      and month = nMSCALC
    ) as SumWithholding,
    (
      select sum(p.sum) 
      from slpays p, slcompcharges ch 
      where p.clnpspfm = f.rn 
      and p.slcompcharges = ch.rn 
      and ch.compch_type = 30 
      and ch.enum_type = 1 
      and year = nYSCALC 
      and month = nMSCALC
    ) as SumPayed,
    (
      select gs.clnrate
      from clnpspfmgs gs 
      where gs.prn = f.rn 
      and gs.grsalary = nRankGs 
      and gs.do_act_from<=dSCALC 
      and (gs.do_act_to is null or gs.do_act_to>=dSCALC)
    ) as RankSalary, 
    (
      select gs.clnrate
      from clnpspfmgs gs 
      where gs.prn = f.rn 
      and gs.grsalary = nPostGs 
      and gs.do_act_from<=dSCALC 
      and (gs.do_act_to is null or gs.do_act_to>=dSCALC)
    ) as PostSalary 
    from clnpspfm f
    inner join clnpersons pr on (f.persrn = pr.rn)
    inner join agnlist a on (pr.pers_agent = a.rn)
    inner join selectlist l on (f.rn = l.document)
    left outer join (
       select r.rn, r.name, ar.prn 
       from agnranks ar, slrank r 
       where ar.rank = r.rn 
       and ar.begin_date<=dSCALC
       and not exists (
           select 1 from agnranks ar1
           where ar1.prn = ar.prn 
           and ar1.begin_date<=dSCALC
           and ar1.begin_date>ar.begin_date
       )
    ) r on (r.prn = pr.pers_agent)
    where l.ident = nIDENT
    order by 1
  ) loop
  
      -- ���� ���� �� ���� ����������
      PKG_XFAST.PROLOGUE
      (
        iTYPE   => PKG_XFAST.DOCUMENT_,
        rHEADER => PKG_XHEADER.WRAP_ALL
                   (
                     sVERSION    => PKG_XHEADER.VERSION_1_0_,
                     sENCODING   => PKG_XHEADER.ENCODING_UTF_,
                     sSTANDALONE => PKG_XHEADER.STANDALONE_NONE_
                   )
      );

      PKG_XFAST.DOWN( 'CalcFormList' ); PKG_XFAST.NODE;
      PKG_XFAST.ATTR( 'xmlns', 'urn:mil.ru:WebCabinetCalculationForm:1.0.0' );
      PKG_XFAST.ATTR( 'xmlns:xs', 'http://www.w3.org/2001/XMLSchema' );

      for k in (
          select distinct 
          ch.numb, ch.name, 
          f_docs_props_get_str_value(nPropERCCode, 'SalaryCompensationsCharges', ch.rn) as ercnumb
          from slpays p, slcompcharges ch 
          where p.clnpspfm = i.rn 
          and p.slcompcharges = ch.rn 
          and ch.compch_type in (10,30) 
          and ch.enum_type = 0 
          and year = nYSCALC and month = nMSCALC
          order by 1
      ) loop
          if not k.ercnumb is null then
            PKG_XFAST.DOWN( 'Kind' ); PKG_XFAST.NODE;
            PKG_XFAST.ATTR( 'ID', trim(k.ercnumb));
            if trim(k.ercnumb) = '1111' then
              PKG_XFAST.ATTR( 'Description', '���������� �������� � ��������� ����������� �� ������ ������ � ������� �������� ������ � ������������ � ��� ����������, � ����� � ������ ���������� � ���������������� �������������� ��� �������������� ���������, � ��� ����� � ���������� ����������');
            else  
              PKG_XFAST.ATTR( 'Description', trim(k.name));
            end if;
            PKG_XFAST.UP;
          end if;
      end loop;
  
      PKG_XFAST.DOWN( 'CalculationForm' ); PKG_XFAST.NODE;
      PKG_XFAST.ATTR( 'ClockNumber', i.tabnum );
      PKG_XFAST.ATTR( 'Rank', i.srank );
      PKG_XFAST.ATTR( 'Year', nYSCALC);
      PKG_XFAST.ATTR( 'Month', nMSCALC);
      PKG_XFAST.ATTR( 'RankSalary', Num2Str(i.srankrate));
      PKG_XFAST.ATTR( 'PostSalary', Num2Str(i.postsalary));
      PKG_XFAST.ATTR( 'RankSalaryCaption', sRankCaption);
      PKG_XFAST.ATTR( 'PostSalaryCaption', sPostCaption);
      PKG_XFAST.ATTR( 'SelfChargeOff', '0.00' );
      PKG_XFAST.ATTR( 'ChildrenChargeOff', '0.00' );
      PKG_XFAST.ATTR( 'PropertyChargeOff', '0.00' );
      PKG_XFAST.ATTR( 'TotalCharge', Num2Str(i.sumcharge));
      PKG_XFAST.ATTR( 'TotalWithholding', Num2Str(i.sumwithholding));
      PKG_XFAST.ATTR( 'TotalPayed', Num2Str(i.sumpayed));
      PKG_XFAST.ATTR( 'ExcessiveWithholding', '0.00' );
      PKG_XFAST.ATTR( 'EmployeeCapacity', '100.00' );
      
      for j in (
         select ch.numb, ch.compch_type, ch.enum_type, 
         f_docs_props_get_str_value(nPropERCCode, 'SalaryCompensationsCharges', ch.rn) as ercnumb,
         ch.name, s.sum, 
         s.bgnfor, s.endfor, s.monthfor, s.yearfor,
         s.rn, 
         trim(sh.pref)||'-'||trim(sh.numb) as docnum,
         sh.docdate 
         from slpays s
         inner join slcompcharges ch on (s.slcompcharges = ch.rn)
         left outer join doclinks l on (l.in_document = s.rn)
         left outer join slpsheets sh on (l.out_document = sh.rn)
         where s.clnpspfm = i.rn
         and s.year = nYSCALC
         and s.month = nMSCALC
         and ch.compch_type in (10, 30)
         and ch.confpay_sign = 0
         and ch.rn = nChargeMain
         order by 1
      )
      loop

          dBegin := to_date(to_char(j.bgnfor,'00')||'.'||to_char(j.monthfor,'00')||'.'||to_char(j.yearfor), 'dd.mm.yyyy');
          begin
           dEnd := to_date(to_char(j.endfor,'00')||'.'||to_char(j.monthfor,'00')||'.'||to_char(j.yearfor), 'dd.mm.yyyy');
          exception
            when others then
            dEnd := last_day(dBegin);  
          end;  
          
          exit;

      end loop;
      
      
      for j in (
         select ch.numb, ch.compch_type, ch.enum_type, 
         f_docs_props_get_str_value(nPropERCCode, 'SalaryCompensationsCharges', ch.rn) as ercnumb,
         ch.name, s.sum, 
         s.bgnfor, s.endfor, s.rn, 
         trim(sh.pref)||'-'||trim(sh.numb) as docnum,
         sh.docdate 
         from slpays s
         inner join slcompcharges ch on (s.slcompcharges = ch.rn)
         left outer join doclinks l on (l.in_document = s.rn)
         left outer join slpsheets sh on (l.out_document = sh.rn)
         where s.clnpspfm = i.rn
         and s.year = nYSCALC
         and s.month = nMSCALC
         and ch.compch_type in (10, 30)
         and ch.confpay_sign = 0
         order by 1
      )
      loop
      
          if j.compch_type = 10 then

            if  j.ercnumb is null then
              continue;
            end if;

             PKG_XFAST.DOWN( 'Charge' ); PKG_XFAST.NODE;
             PKG_XFAST.ATTR( 'KindID', trim(j.ercnumb) );
             
             PKG_XFAST.ATTR( 'BeginDate', to_char(dBegin,'dd.mm.yyyy') );
             PKG_XFAST.ATTR( 'EndDate', to_char(dEnd,'dd.mm.yyyy') );
             
             PKG_SLPAYSPRM.GET(j.rn, 'WRK', nWRK);
             PKG_SLPAYSPRM.GET(j.rn, 'WRKMEA', nFOVTYPE);
             PKG_SLPAYSPRM.GET(j.rn, 'PRC', nPRC);
             
             if (not nPRC is null) and (trim(j.ercnumb) <> '1207') and (trim(j.ercnumb) <> '1120') then
               PKG_XFAST.ATTR( 'UnitName', '%' );
               PKG_XFAST.ATTR( 'Quantity', Num2Str(nPRC) );
             end if;
             
          elsif j.enum_type = 0 then
             PKG_XFAST.DOWN( 'Withholding' ); PKG_XFAST.NODE;
             PKG_XFAST.ATTR( 'KindID', trim(j.ercnumb) );
             PKG_XFAST.ATTR( 'BeginDate', to_char(dBegin,'dd.mm.yyyy') );
             PKG_XFAST.ATTR( 'EndDate', to_char(dEnd,'dd.mm.yyyy') );
          else 
             PKG_XFAST.DOWN( 'Payment' ); PKG_XFAST.NODE;
             if j.docnum is null then
               PKG_XFAST.ATTR( 'Description', trim(j.name));
             else  
               PKG_XFAST.ATTR( 'Description', '����������� � ���� � '||j.docnum||' �� '||to_char(j.docdate,'dd.mm.yyyy'));
             end if;
          end if;
          
          PKG_XFAST.ATTR( 'Sum', Num2Str(j.sum) );
          
          PKG_XFAST.UP; -- �������� ���
        
      end loop;
      
      PKG_XFAST.DOWN( 'Uchr' ); PKG_XFAST.NODE;
      PKG_XFAST.ATTR( 'Data', '��� "��� �� �� �� ���"' );
      PKG_XFAST.UP;
      
      PKG_XFAST.DOWN( 'Tlfu' ); PKG_XFAST.NODE;
      PKG_XFAST.ATTR( 'Data', '+7(4212)39-51-53' );
      PKG_XFAST.UP;
      
      PKG_XFAST.UP; -- CalculationForm
      PKG_XFAST.UP; -- CalcFormList
      
      sFILENAME := 'cf_'||to_char(sysdate, 'yyyymmdd_HH24Miss')||'_'||i.tabnum||'_001.xml';
      PKG_XFAST.SERIALIZE_TO_BFILE(nOUTIDENT, sFILENAME, null, null);

      -- �������� XML
      PKG_XFAST.EPILOGUE;
      
      -- �������������� ����
      
      PKG_XFAST.PROLOGUE
      (
        iTYPE   => PKG_XFAST.DOCUMENT_,
        rHEADER => PKG_XHEADER.WRAP_ALL
                   (
                     sVERSION    => PKG_XHEADER.VERSION_1_0_,
                     sENCODING   => PKG_XHEADER.ENCODING_UTF_,
                     sSTANDALONE => PKG_XHEADER.STANDALONE_NONE_
                   )
      );

      PKG_XFAST.DOWN( 'AuthenticationData' ); PKG_XFAST.NODE;
      PKG_XFAST.ATTR( 'xmlns', 'urn:mil.ru:WebCabinetAuthentication:1.0.0' );
      
      PKG_XFAST.DOWN( 'Item' ); PKG_XFAST.NODE;
      PKG_XFAST.ATTR( 'ClockNumber', i.tabnum );

      sNomer := trim(i.pers_note);
      sNomer := replace(sNomer, chr(10), '');
      sNomer := replace(sNomer, chr(13), '');
      sHashV := sNomer||to_char(i.agnburn,'dd.mm.yyyy');
      sHashV := CONVERT(sHashV,'UTF8');
      sHash := utl_raw.cast_to_raw(sHashV); 
      sHash := dbms_crypto.hash(sHash, dbms_crypto.hash_sh1);
      sHashV := utl_raw.cast_to_varchar2(utl_encode.base64_encode(sHash));
      PKG_XFAST.ATTR( 'Hash', sHashV );

      PKG_XFAST.UP; -- Item
      PKG_XFAST.UP; -- AuthenticationData

      sFILENAME := 'auth_'||to_char(sysdate, 'yyyymmdd_HH24Miss')||'_'||i.tabnum||'_001.xml';
      PKG_XFAST.SERIALIZE_TO_BFILE(nOUTIDENT, sFILENAME, null, null);

      -- �������� XML
      PKG_XFAST.EPILOGUE;

  end loop;


end;
/
