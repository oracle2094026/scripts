drop table agncontracts_12032023

create table agncontracts_12032023 as select * from agncontracts

update agncontracts set PRCNTKND = 28181293

select * from v_agncontracts where CNTRNUMB = '111111'
select * from PRCONTRACT

insert into PRCONTRACT (rn, company, crn, doc_type, doc_pref, Doc_Numb, doc_year, doc_date, owner, owner_agent, Agncontracts)
select gen_id(), 23647, a.crn, 28181478, '', trim(a.cntrnumb), 0, nvl(a.datecntr, a.datebeg), 0, a.owner_agent, a.rn
from agncontracts a where not exists (select 1 from PRCONTRACT p where p.agncontracts = a.rn);


select /*gen_id(),*/ 23647, a.crn, 28181478, '', trim(a.cntrnumb), 0, nvl(a.datecntr, a.datebeg), 0, a.owner_agent, a.rn
from agncontracts a where not exists (select 1 from PRCONTRACT p where p.agncontracts = a.rn)
order by 5


select * from agncontracts where rn = 91437725

    insert into AGNCONTRACTS (rn, PRN, version, CRN, CNTRNUMB, DATEBEG, PRCNTKND, OWNER_AGENT)
    values (nrn, nprn, 23775, ncrn, stabnum, ddatebegin, 28181293, 8997638);
    
    /*insert into PRCONTRACT (rn, company, crn, doc_type, doc_pref, Doc_Numb, doc_year, doc_date, owner, owner_agent, Agncontracts)
    select gen_id(), 23647, a.crn, 28181478, '', trim(a.cntrnumb), 0, nvl(a.datecntr, a.datebeg), 0, a.owner_agent, a.rn
    from agncontracts a where a.rn = nrn;*/


begin
  for i in (
    select f.rn, f.begeng, f.endeng, f.agncontracts,
    trim(to_char(td.num)) as num, td.contract_date, t.code, td.contract_type, 
    td.substitution_person_rn, td.possible_end, td.reason_rn, td.period_to
    from clnpspfm f, viv_trudog td, clnpspfmtypes t, agncontracts ac
    where f.persrn = td.clientperson_rn and f.psdeprn = td.clientpostdepart_rn
    and f.begeng = td.period_from and nvl(td.additional_contract_mark,0) = 0
    and f.clnpspfmtypes = t.rn
    and (
      (td.contract_type = 0 and t.is_primary = 1 and lower(t.code) like '���%')
      or (td.contract_type = 1 and t.is_primary = 1 and lower(t.code) like '����%')
      or (td.contract_type = 2 and t.is_primary = 0 and not lower(t.code) like '����%')
      or (td.contract_type = 3 and t.is_primary = 0 and lower(t.code) like '����%')
    )
    and (
      not td.substitution_person_rn is null
      or
      not td.possible_end is null 
      or
      not td.reason_rn is null 
      or
      not td.period_to is null
    )
    and ac.rn = f.agncontracts
    and ac.PRCNTKND = 28181293
    order by CONTRACT_DATE
  ) loop

     update agncontracts set PRCNTKND = 91384289 where rn = i.agncontracts;

  end loop;
end;

-- ���������� ��������� �� �������� ���������

declare
nrn number;
begin
  for i in (
    select trim(to_char(td.num)) as num, td.unique_num, td.contract_date, td.contract_type, 
    td.substitution_person_rn, td.possible_end, td.reason_rn, td.period_to,
    a.rn as agnlistrn, a.crn as agnlistcrn, 
    (case when
     (
      not td.substitution_person_rn is null
      or
      not td.possible_end is null 
      or
      not td.reason_rn is null 
      or
      not td.period_to is null
    ) then 1 else 0 
    end) istemp
    
    from viv_trudog td, clnpersons p, agnlist a
    where td.clientperson_rn = p.rn and p.pers_agent = a.rn
    and nvl(td.additional_contract_mark,0) = 0
    and not exists 
    (select 1 from AGNCONTRACTS ac where ac.CNTRNUMB = td.unique_num)
    order by 1
  ) loop

    nrn := gen_id();
    
    if i.istemp = 1 then
      insert into AGNCONTRACTS (rn, PRN, version, CRN, CNTRNUMB, DATEBEG, PRCNTKND, OWNER_AGENT)
      values (nrn, i.agnlistrn, 23775, i.agnlistcrn, i.unique_num, i.contract_date, 91384289, 8997638);
      
      insert into PRCONTRACT (rn, company, crn, doc_type, doc_pref, Doc_Numb, doc_year, doc_date, owner, owner_agent, Agncontracts)
      select gen_id(), 23647, a.crn, 28181478, '���', trim(a.cntrnumb), 0, nvl(a.datecntr, a.datebeg), 0, a.owner_agent, a.rn
      from agncontracts a where a.rn = nrn;
      
    else

      insert into AGNCONTRACTS (rn, PRN, version, CRN, CNTRNUMB, DATEBEG, PRCNTKND, OWNER_AGENT)
      values (nrn, i.agnlistrn, 23775, i.agnlistcrn, i.unique_num, i.contract_date, 28181293, 8997638);
      
      insert into PRCONTRACT (rn, company, crn, doc_type, doc_pref, Doc_Numb, doc_year, doc_date, owner, owner_agent, Agncontracts)
      select gen_id(), 23647, a.crn, 28181478, '��', trim(a.cntrnumb), 0, nvl(a.datecntr, a.datebeg), 0, a.owner_agent, a.rn
      from agncontracts a where a.rn = nrn;
      
    end if;  
    

  end loop;
end;

delete from AGNCONTRACTS where rn in (select AGNCONTRACTS from PRCONTRACT where trim(doc_pref) = '��')

select * from PRCONTRACT where doc_pref = '��'

-- ����������� ��������� � �����������

CREATE FUNCTION is_number( p_str IN VARCHAR2 )
  RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE
IS
  l_num NUMBER;
BEGIN
  l_num := to_number( p_str );
  RETURN 'Y';
EXCEPTION
  WHEN value_error THEN
    RETURN 'N';
END is_number;


CREATE FUNCTION try_to_number( p_str IN VARCHAR2 )
  RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE
IS
  l_num NUMBER;
BEGIN
  l_num := to_number( p_str );
  RETURN l_num;
EXCEPTION
  WHEN value_error THEN
    RETURN 0;
END;



create table clnpspfm_16032023 as select * from clnpspfm

update clnpspfm set agncontracts = (select agncontracts from clnpspfm_16032023 f where f.rn = clnpspfm.rn)

begin
  for i in (
      select * from clnpspfm_16032023
  ) loop
    update clnpspfm set agncontracts = i.agncontracts where rn = i.rn;
  end loop;
  
  for i in (
      select * from CLNPERSVAC_17032023
  ) loop
    update CLNPERSVAC set agncontracts = i.agncontracts where rn = i.rn;
  end loop;
  
end;

select to_number('  0001') from dual;

select F_DOCS_PROPS_GET_STR_VALUE( 62145894,'ClientPostPerform', f.rn) 
from clnpspfm f where is_number(F_DOCS_PROPS_GET_STR_VALUE( 62145894,'ClientPostPerform', f.rn)) = 'N'

select * from viv_trudog td where td.unique_num like '%�/�%'
select * from viv_trudog td where td.num is null
select * from viv_trudog td where td.num = 0

begin
  for i in (
    select f.rn as clnpspfm, a.rn as agnlist, td.unique_num, ac.rn as AGNCONTRACTS
    from clnpspfm f, viv_trudog td, clnpspfmtypes t, clnpersons p, agnlist a, agncontracts ac
    where try_to_number(F_DOCS_PROPS_GET_STR_VALUE( 62145894,'ClientPostPerform', f.rn)) = td.num
    and f.persrn = td.clientperson_rn and f.psdeprn = td.clientpostdepart_rn
    --and f.begeng = td.period_from 
    and nvl(td.additional_contract_mark,0) = 0
    and f.persrn = p.rn and p.pers_agent = a.rn
    and ac.prn = a.rn and ac.cntrnumb = td.unique_num
    and f.clnpspfmtypes = t.rn
    /*and (
      (td.contract_type = 0 and t.is_primary = 1 and lower(t.code) like '���%')
      or (td.contract_type = 1 and t.is_primary = 1 and lower(t.code) like '����%')
      or (td.contract_type = 2 and t.is_primary = 0 and not lower(t.code) like '����%')
      or (td.contract_type = 3 and t.is_primary = 0 and lower(t.code) like '����%')
    )*/
  ) loop
  
     update clnpspfm set AGNCONTRACTS = i.AGNCONTRACTS where rn = i.clnpspfm;

  end loop;
end;


begin
  for i in (
      select distinct f1.agncontracts as oldcontract, f.agncontracts as contract
      from clnpspfm_16032023 f1, clnpspfm f 
      where f.rn = f1.rn
      and f1.agncontracts<>f.agncontracts
  ) loop
  
    update CLNPERSVAC set agncontracts = i.contract where agncontracts = i.oldcontract;

  end loop;
end;

create table CLNPERSVAC_17032023 as select *  from CLNPERSVAC
