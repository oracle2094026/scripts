CREATE OR REPLACE PROCEDURE "P_ORGEDUSERT_KHV" (dDateBegin date, dDateEnd date, sORGs varchar2, sCateg varchar2, sPost varchar2, sSpec varchar2, nOrg number)
as
 iLine number;
 sOrgss varchar2(2000);
begin

  if (sORGs is null and not nOrg is null) then
    select name into sOrgss from Companies where rn = nOrg;
  else
    sOrgss :=  sORGs;
  end if;

/* ������ */
  PRSG_EXCEL.PREPARE;

  /* ��������� �������� �������� ����� */
  PRSG_EXCEL.SHEET_SELECT( '������'  );

  /* �������� */

  PRSG_EXCEL.LINE_DESCRIBE( '������' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'NUM' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'COMPANY' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'FIO' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DR' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SNILS' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SPODR' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SDOL' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'SSTA' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'STA' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'PODR' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DOL' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DNACH' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'DKON' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'VIDISP' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'CATEG' );

  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'VUZ' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'EDUFROM' );
  PRSG_EXCEL.LINE_CELL_DESCRIBE( '������', 'EDUSPEC' );

  for i in (

      select * from
      (
            select
            (select name from companies c where c.rn = p.company) as company_name,
            al.agnfamilyname||' '||al.agnfirstname||' '||al.agnlastname as agnname,
            to_char(al.agnburn,'dd.mm.yyyy') as agnburn,
            al.pension_nbr,
            (select d.name from CLNPSDEP dp, ins_department D where f.psdeprn = dp.rn and dp.deptrn = d.rn) as staff_dep,
            (select PSDEP_NAME from CLNPSDEP dp where f.psdeprn = dp.rn) as staff_post,
            (select dphs.rateacc from CLNPSDEPHS dphs where f.psdeprn = dphs.prn and dphs.do_act_from = (  select max(do_act_from) from CLNPSDEPHS dphs1 where dphs1.prn = f.psdeprn )  ) as staff_post_rate,
            (select fh.rateacc from  CLNPSPFMHS fh where fh.prn = f.rn and fh.do_act_from = (select max(fh1.do_act_from) from CLNPSPFMHS fh1 where fh1.prn = f.rn)) as pfm_rate,
            (select name from ins_department d where d.rn = f.deptrn) as dep_name,
            (select name from clnposts ps where ps.rn = f.postrn) as post_name,
            f.begeng,
            f.endeng,
            t.name as type_name,
            o.code as type_code,
            f.note,

            crt.certif_date as certif_date,
            (select i.Name from EDINSTITUT i where rn = crt.edinstitut) as educ_inst,
            (select name from PRPROF p where p.rn = crt.prprof) eduspec

            from CLNPERSONS p, agnlist al, CLNPSPFM f, clnpspfmtypes t, officercls o, CLNPERSCERTIF crt

            where p.pers_agent = al.rn
            and f.persrn = p.rn
            and t.rn = f.clnpspfmtypes(+)
            and o.rn = f.officercls
            and crt.prn = p.rn
            and (sCateg is null or lower(o.name) like '%'||lower(sCateg)||'%')
            and (sOrgss is null or p.company in (select rn from companies where instr(sOrgss,name)>0))
            and (f.endeng is null or f.endeng >= dDateBegin)
            and (f.begeng <= dDateEnd)
            order by 1, 11, 12, 2
      ) s1  where (sPost is null or lower(nvl(staff_post, post_name)) like '%'||lower(sPost)||'%')
      and (sSpec is null or Instr(lower(s1.eduspec), lower(sSpec))>0)

      ) loop

            iLine := PRSG_EXCEL.LINE_APPEND('������');
            PRSG_EXCEL.CELL_VALUE_WRITE( 'NUM', 0, iLine, iLine);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'COMPANY' , 0, iLine, i.company_name);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'FIO' , 0, iLine, i.agnname);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DR' , 0, iLine, i.agnburn);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SNILS' , 0, iLine, i.pension_nbr);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'SPODR' , 0, iLine, i.staff_dep);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SDOL' , 0, iLine, i.staff_post);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'SSTA' , 0, iLine, i.staff_post_rate);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'STA' , 0, iLine, i.pfm_rate);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'PODR' , 0, iLine, i.dep_name);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DOL' , 0, iLine, i.post_name);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DNACH' , 0, iLine, i.begeng);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'DKON' , 0, iLine, i.endeng);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'VIDISP' , 0, iLine, i.type_name);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'CATEG' , 0, iLine, i.type_code);

            PRSG_EXCEL.CELL_VALUE_WRITE( 'VUZ' , 0, iLine, i.educ_inst);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'EDUFROM' , 0, iLine, i.certif_date);
            PRSG_EXCEL.CELL_VALUE_WRITE( 'EDUSPEC' , 0, iLine, i.eduspec);

    end loop;

    PRSG_EXCEL.LINE_DELETE('������',0);

end;
/
